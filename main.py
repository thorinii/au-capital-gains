import csv
import datetime
from decimal import Decimal
from typing import Dict, Generator, List, NamedTuple, Optional, Tuple


class Txn(NamedTuple):
    date: datetime.date
    code: str
    units: int
    value: Decimal


class Bundle(NamedTuple):
    code: str
    id: int
    acquired: datetime.date
    units: int
    unit_price: Decimal


class SaleEvent(NamedTuple):
    code: str
    bundle_id: int
    date: datetime.date
    age: datetime.timedelta
    units: int
    original_value: Decimal
    sale_value: Decimal


class GainLoss(NamedTuple):
    date: datetime.date
    gain: Decimal
    loss: Decimal


def main() -> None:
    txns = list(get_txns())
    txns.sort(key=lambda t: t.date)

    txns_by_code = group_by_code(txns)
    sales_by_code = {
        code: list(track_state(code, ts)) for code, ts in txns_by_code.items()
    }
    yearly_gain_loss_by_code = {
        code: {gl.date: gl for gl in track_gain_loss(code, sales)}
        for code, sales in sales_by_code.items()
    }
    yearly_gain_loss = sum_gain_loss(yearly_gain_loss_by_code)

    for grand_total in yearly_gain_loss:
        date = grand_total.date
        year = date.year
        print("=" * 80)
        print(f"Report for FY {year}/{year + 1}")
        print()

        print(f"{'Code':>5} {'Gain':>9} {'Loss':>9}")
        print("-" * 25)
        for code, gain_losses in yearly_gain_loss_by_code.items():
            if date not in gain_losses:
                continue
            _, gain, loss = gain_losses[date]
            gain_s = f"${gain}" if gain else '-'
            loss_s = f"${loss}" if loss else '-'
            print(f"{code:>5} {gain_s:>9} {loss_s:>9}")

        _, gain, loss = grand_total
        gain_s = f"${gain}" if gain else '-'
        loss_s = f"${loss}" if loss else '-'
        print("-" * 25)
        print(f"{'TOTAL':>5} {gain_s:>9} {loss_s:>9}")
        print()


def get_txns() -> Generator[Txn, None, None]:
    with open("Transactions_3420916_01062018_31072021.csv", "r") as fp:
        reader = csv.reader(fp)

        header = True
        for row in reader:
            if header:
                header = False
                continue

            day, month, year = row[0].split("/")
            date = datetime.date(int(year), int(month), int(day))

            description = row[2]
            if description[0] in ("B", "S"):
                type, units_s, code, _, unit_price = description.split(" ")
                units = int(units_s)
                if type == "B":
                    diff = Decimal(row[3])
                else:
                    units = -units
                    diff = -Decimal(row[4])
                yield Txn(date, code, units, diff)
            else:
                print("?", description)


def group_by_code(txns: List[Txn]) -> Dict[str, List[Txn]]:
    codes: Dict[str, List[Txn]] = dict()
    for txn in txns:
        if txn.code not in codes:
            codes[txn.code] = []
        codes[txn.code].append(txn)
    return codes


def track_state(code: str, txns: List[Txn]) -> Generator[SaleEvent, None, None]:
    bundles: List[Bundle] = []

    bundle_index = 0
    for txn in txns:
        if txn.units > 0:
            unit_price = txn.value / txn.units
            bundles.append(Bundle(code, bundle_index, txn.date, txn.units, unit_price))
            bundle_index += 1
        elif txn.units < 0:
            if not bundles:
                raise Exception(
                    f"No bundles available to sell for {code} on {txn.date}"
                )

            tmp_selling = -txn.units

            head: Optional[Bundle] = bundles[0]
            while head and head.units <= tmp_selling:
                bundles.pop(0)
                yield SaleEvent(
                    code,
                    date=txn.date,
                    age=txn.date - head.acquired,
                    bundle_id=head.id,
                    units=head.units,
                    original_value=head.unit_price * head.units,
                    sale_value=(txn.value / txn.units * head.units),
                )
                tmp_selling -= head.units

                head = bundles[0] if bundles else None

            if head and tmp_selling:
                bundles[0] = head._replace(units=head.units - tmp_selling)
                yield SaleEvent(
                    code,
                    date=txn.date,
                    age=txn.date - head.acquired,
                    bundle_id=head.id,
                    units=tmp_selling,
                    original_value=head.unit_price * tmp_selling,
                    sale_value=(txn.value / txn.units * tmp_selling),
                )


def track_gain_loss(
    code: str, sales: List[SaleEvent]
) -> Generator[GainLoss, None, None]:
    gains = Decimal()
    losses = Decimal()

    fy: Optional[datetime.date] = None
    for event in sales:
        if not fy:
            fy = get_fy(event.date)

        if fy and fy != get_fy(event.date):
            yield GainLoss(fy, gains, losses)
            gains = Decimal()
            losses = Decimal()
            fy = get_fy(event.date)

        days = int(event.age / datetime.timedelta(days=1))
        original_value = event.original_value.quantize(Decimal("1.00"))
        sale_value = event.sale_value.quantize(Decimal("1.00"))

        if sale_value > original_value:
            gains += sale_value - original_value
        elif sale_value < original_value:
            losses += original_value - sale_value

    if fy and (gains or losses):
        yield GainLoss(fy, gains, losses)


def get_fy(date: datetime.date) -> datetime.date:
    if date.month > 6:
        return date.replace(month=7, day=1)
    else:
        return date.replace(year=date.year - 1, month=7, day=1)


def sum_gain_loss(
    yearly_gain_loss_by_code: Dict[str, Dict[datetime.date, GainLoss]]
) -> List[GainLoss]:
    years: Dict[datetime.date, GainLoss] = {}

    for code, gain_losses in yearly_gain_loss_by_code.items():
        for gain_loss in gain_losses.values():
            date, gain, loss = gain_loss
            if date in years:
                acc = years[date]
                years[date] = acc._replace(gain=acc.gain + gain, loss=acc.loss + loss)
            else:
                years[date] = gain_loss

    totals = [gl for gl in years.values()]
    totals.sort(key=lambda gl: gl.date)
    return totals


main()
